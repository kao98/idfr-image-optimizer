<?php

/* 
 * knt/image-optimizer: a small library to easily resize and optimize images
 * 
 * Licensed under The MIT License
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

require_once '../ImageOptimizer.php';
use \Knt\Image\Optimizer;

/**
 * This file contains some functional tests to allow some visual verification
 * of the optimizer. Just browse to this file (ie: http://localhost/tests/test.php).
 */

if (($imageSource = filter_input(INPUT_GET, 'imgsrc'))) {
    
    // This is the postback: we returned optimized pictures
    
    if ($imageSource === 'http://reho.st/www.icone-gif.com/gif/drapeaux/france/france-gif-112.gif') {
        echo Optimizer\ImageOptimizer::optimize($imageSource, Optimizer\ImageOptimizer::SIZE_FIXED, 64, 64);
    } else {
        echo Optimizer\ImageOptimizer::optimize($imageSource, Optimizer\ImageOptimizer::SIZE_RESIZE, 1200, 500);
    }
    
} else {
        
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Knt\ImageOptimizer - basic tests</title>
    </head>
    <body>
        <h1>Basic tests of the image optimizer</h1>
        <h2>Pitures are displayed 2 times:</h2>
        <ol>
            <li>
                The first time it is the original picture
            </li>
            <li>
                The second time is is optimized by the library
            </li>
        </ol>
        
        <hr />
        <h3>A jpeg, resized at a maximum width of 1200px</h3>
        <img src="http://www.kaonet-fr.net/blog/img/bkg.jpg" />
        <br />
        <img src="test.php?imgsrc=http://www.kaonet-fr.net/blog/img/bkg.jpg" />
        
        <hr />
        <h3>A gif, resized on a fixe size of 64x64 pixels</h3>
        <img src="http://reho.st/www.icone-gif.com/gif/drapeaux/france/france-gif-112.gif" />
        <br />
        <img src="test.php?imgsrc=http://reho.st/www.icone-gif.com/gif/drapeaux/france/france-gif-112.gif" />
        
        <hr />
        <h3>A png, resized at a maximum height of 500px</h3>
        <img src="http://reho.st/https://drapeaux-flags.com/images/drapeaux/png_norm/FRVBESANCON.png" />
        <br />
        <img src="test.php?imgsrc=http://reho.st/https://drapeaux-flags.com/images/drapeaux/png_norm/FRVBESANCON.png" />
    
    </body>
</html>

<?php
}