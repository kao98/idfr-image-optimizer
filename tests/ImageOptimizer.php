<?php

/* 
 * knt/image-optimizer: a small library to easily resize and optimize images
 * 
 * Licensed under The MIT License
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace Knt\Image\Optimizer\tests\units;

require_once 'atoum.phar';
require_once '../ImageOptimizer.php';

use \Knt\Image\Optimizer;


/**

    Ecrivez une fonction PHP qui retourne une image optimisée à partir d’une
    image en entrée et des paramètres suivants :

    - Image : url de l’image source

    - Type de fonctionnement : peut avoir les valeurs
      - type=fixe : l’image qui sera retournée devra avoir une taille fixe,
        définie par les valeurs x et y (les 2 valeurs étant obligatoires)
      - type=resize : l’image qui sera retournée devra avoir comme taille
        maximum x et/ou y

    - x : largeur
    - y : hauteur

 */
class ImageOptimizer extends \mageekguy\atoum\test
{
    /**
     * Optimize must return an image.
     * The image type must be the same as the given one for jpg, gif and png.
     */
    public function testOptimize() {

    // Given a jpeg, a gif and a png pictures
        $input = array (
            IMAGETYPE_JPEG => 'http://www.kaonet-fr.net/blog/img/bkg.jpg',
            IMAGETYPE_GIF  => 'http://reho.st/www.icone-gif.com/gif/drapeaux/france/france-gif-112.gif',
            IMAGETYPE_PNG  => 'http://reho.st/https://drapeaux-flags.com/images/drapeaux/png_norm/FRVBESANCON.png'
        );

    // When applying the ImageOptimizer to them
        foreach ($input as $imageType => $imageSrc) {
            $output = Optimizer\ImageOptimizer::optimize($imageSrc);
            $info   = getimagesizefromstring($output);

    // Then resulting pictures are the same type of the given ones

            $this
                ->integer($info[2])
                    ->isEqualTo($imageType)
            ;

        }

    }

    /**
     * Optimize must throw an InvalidArgumentException if trying to optmize
     * a picture in a format that is not a jpeg, gif or png
     */
    public function testOptimizeExceptOnInvalidType() {
        
    // Given a picture not taken in charge by the library
        $input = 'http://www.osadl.org/fileadmin/favicon.ico';

    // When applying the ImageOptimizer to them
        
        $this
            ->exception(
                function () use ($input) {
                    Optimizer\ImageOptimizer::optimize($input);
                }
            
    // Then an exception is raised           
            )
                ->isInstanceOf('\InvalidArgumentException')
                ->hasMessage('Unrecognized image type. Image must be a jpeg, gif, or png.')
        ;
    }

    /**
     * Optimize must return an image scaled to the given x and y values.
     */
    public function testScale() {

    // Given some valid pictures
        $input = array (
            'http://www.kaonet-fr.net/blog/img/bkg.jpg',
            'http://reho.st/www.icone-gif.com/gif/drapeaux/france/france-gif-112.gif',
            'http://reho.st/https://drapeaux-flags.com/images/drapeaux/png_norm/FRVBESANCON.png'
        );

    // When resizing them with the FIXED parameter and correct x and y values
        foreach ($input as $imageSrc) {
            $output = Optimizer\ImageOptimizer::optimize($imageSrc, Optimizer\ImageOptimizer::SIZE_FIXED, 64, 64);
            $info   = getimagesizefromstring($output);

    //Then resulting pictures are the expected size

            $this
                ->integer($info[0])
                    ->isEqualTo(64)
                ->integer($info[1])
                    ->isEqualTo(64)
            ;

        }

    }

    /**
     * Optimize must return an image resized to the given x or y values max.
     */
    public function testResize() {

    // Given some valid pictures
        $input = array (
            'http://www.kaonet-fr.net/blog/img/bkg.jpg',
            'http://reho.st/www.icone-gif.com/gif/drapeaux/france/france-gif-112.gif',
            'http://reho.st/https://drapeaux-flags.com/images/drapeaux/png_norm/FRVBESANCON.png'
        );
        
        // Those value will allow to test all code path:
        // The jpeg will be resized to a width of 1200,
        // The png will be resized to a height of 500,
        // and the gif won't be resized
        $newWidth   = 1200;
        $newHeight  = 500;

    // When resizing them the RESIZE parameter and valid values for x and y
        foreach ($input as $imageSrc) {
            $output = Optimizer\ImageOptimizer::optimize($imageSrc, Optimizer\ImageOptimizer::SIZE_RESIZE, $newWidth, $newHeight);
            $info   = getimagesizefromstring($output);

    //Then the resulting pictures are not wider nor heiger than expected

            $this
                ->integer($info[0])
                    ->isLessThanOrEqualTo($newWidth)
                ->integer($info[1])
                    ->isLessThanOrEqualTo($newHeight)
            ;

        }
    }
    
    /**
     * Optimize must throw an InvalidArgumentException if trying to scale a
     * picture with a new width lower than or equal to 0.
     */
    public function testScaleExceptOnInvalidX() {
        
    // Given a valid picture
        $input = 'http://www.kaonet-fr.net/blog/img/bkg.jpg';

    // When trying to scale it but not specifying x
        
        $this
            ->exception(
                function () use ($input) {
                    Optimizer\ImageOptimizer::optimize($input, Optimizer\ImageOptimizer::SIZE_FIXED, 0, 10);
                }
            
    // Then an exception is raised        
            )
                ->isInstanceOf('\InvalidArgumentException')
                ->hasMessage('Stretch method (using SIZE_FIXED) requires $x and $y to be both positive integer values.')
        ;
    }
    
    /**
     * Optimize must throw an InvalidArgumentException if trying to scale a
     * picture with a missing height.
     */
    public function testScaleExceptOnMissingY() {
        
    // Given a valid picture
        $input = 'http://www.kaonet-fr.net/blog/img/bkg.jpg';

    // When trying to scale it but not specifying y
        
        $this
            ->exception(
                function () use ($input) {
                    Optimizer\ImageOptimizer::optimize($input, Optimizer\ImageOptimizer::SIZE_FIXED, 10);
                }
            
    // Then an exception is raised        
            )
                ->isInstanceOf('\InvalidArgumentException')
                ->hasMessage('Stretch method (using SIZE_FIXED) requires $x and $y to be both positive integer values.')
        ;
    }
    
    /**
     * Optimize must throw an InvalidArgumentException if trying to scale a
     * picture with a new height lower than or equal to 0.
     */
    public function testScaleExceptOnInvalidY() {
        
    // Given a valid picture
        $input = 'http://www.kaonet-fr.net/blog/img/bkg.jpg';

    // When trying to scale it but not specifying y
        
        $this
            ->exception(
                function () use ($input) {
                    Optimizer\ImageOptimizer::optimize($input, Optimizer\ImageOptimizer::SIZE_FIXED, 10, -5);
                }
            
    // Then an exception is raised        
            )
                ->isInstanceOf('\InvalidArgumentException')
                ->hasMessage('Stretch method (using SIZE_FIXED) requires $x and $y to be both positive integer values.')
        ;
    }

}
