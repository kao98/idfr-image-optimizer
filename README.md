## An image optimizer function, written in php

A php function to resize or scale jpeg, gif or png pictures.
See the tests files for details and samples. 
 
Browse to http://www.your-server.example.net/tests/test.php to see it in action. 
 
To run unit tests, from the tests directory:  
` $ php ./atoum.phar ./ImageOptimizer.php`
