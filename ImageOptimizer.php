<?php

/* 
 * knt/image-optimizer: a small library to easily resize and optimize images
 * 
 * Licensed under The MIT License
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace Knt\Image\Optimizer;

/**
 * ImageOptimizer provide an Optimize static method to resize a jpeg/gif/png
 * picture and eventually to optimize it if possible.
 */
class ImageOptimizer
{
    
    const SIZE_ORIGINAL = '';       //Image size isn't altered
    const SIZE_FIXED    = 'fixe';   //Image size is fully specified
    const SIZE_RESIZE   = 'resize'; //Image size must not exceed x and/or y
    
    // The following members allow to abstract imagecreate*** and image***
    // gd functions.
    private static $imageCreateFunction = null;
    private static $imageRenderFunction = null;
    
    /**
     * 
     * @param string $imageUrl
     * @param string $type
     * @param int $x
     * @param int $y
     * @return string the resized and optimized picture
     */
    public static function optimize($imageUrl, $type = ImageOptimizer::SIZE_ORIGINAL, $x = 0, $y = 0) {
        
        $imageInfo = getimagesize($imageUrl);
        $imageType = $imageInfo[2];
        
        self::prepareFunctions($imageType);
        
        // anonymous function defined within class members cannot be called
        // directly. We must use regular variable instead.
        $imageCreateFunction = self::$imageCreateFunction;
        $imageRenderFunction = self::$imageRenderFunction;
        
        $image = $imageCreateFunction($imageUrl);
        
        switch ($type) {
            
        case ImageOptimizer::SIZE_FIXED:
            self::scale($image, $x, $y);
            break;
        
        case ImageOptimizer::SIZE_RESIZE:
            self::resize($image, $x, $y);
            break;
        }
        
        // Usage of output buffering to allow at least some unit tests
        ob_start();
        $imageRenderFunction($image);
        return ob_get_clean();
        
    }
    
    /**
     * Prepare the anonymous imageCreateFunction and imageRenderFunction
     * @param int $imageType one of the IMAGETYPE_xxx constant
     * @see http://php.net/manual/en/image.constants.php 
     */
    private static function prepareFunctions($imageType) {
        
        self::$imageCreateFunction = function () {
            throw new \InvalidArgumentException(
                'Unrecognized image type. Image must be a jpeg, gif, or png.');
        };
        
        self::$imageRenderFunction = function () {};
        
        switch ($imageType) {
        case IMAGETYPE_JPEG:
            self::$imageCreateFunction = self::imageCreateFromJpeg();
            self::$imageRenderFunction = self::imageRenderAsJpeg();
            break;
        
        case IMAGETYPE_PNG:
            self::$imageCreateFunction = self::imageCreateFromPng();
            self::$imageRenderFunction = self::imageRenderAsPng();
            break;
        
        case IMAGETYPE_GIF:
            self::$imageCreateFunction = self::imageCreateFromGif();
            self::$imageRenderFunction = self::imageRenderAsGif();
            break;
        
        }
        
    }
    
    /**
     * scale the picture to the given width ($x) and height ($y)
     * @param res $image
     * @param int $x
     * @param int $y
     * @throws \InvalidArgumentException
     */
    public static function scale(&$image, $x, $y) {
        if ($x <= 0 || $y <= 0) {
            throw new \InvalidArgumentException(
                'Stretch method (using SIZE_FIXED) requires $x and $y to be ' .
                'both positive integer values.');
        }
        
        // We first set interpolation before scaling because of the 'mode'
        // argument of the imagescale function not working in php 5.5.9.
        \imagesetinterpolation($image, IMG_NEAREST_NEIGHBOUR);
        $image = \imagescale($image, $x, $y);
    }
    
    /**
     * Set the witdh and the height of the picture to be smaller that the given
     * width ($x) and height ($y)
     * @param res $image
     * @param int $x
     * @param int $y
     */
    public static function resize(&$image, $x, $y) {
        $w = imagesx($image);
        $h = imagesy($image);
        
        if ($w <= $x && $h <= $y) {
            return;
        }
        
        if ($w > $x) {
            self::scale($image, $x, $h * ($x / $w));
        } else {
            self::scale($image, $w * ($y / $h), $y);
        }
    }
    
    /**
     * Return the function that will create an image res from a jpeg file
     * @return function
     */
    private static function imageCreateFromJpeg() {
        return function($fileName) { return \imagecreatefromjpeg($fileName); };
    }
    
    /**
     * Return the function that will create an image res from a gif file
     * @return function
     */
    private static function imageCreateFromGif() {
        return function($fileName) { return \imagecreatefromgif($fileName); };
    }
    
    /**
     * Return the function that will create an image res from a png file
     * @return function
     */
    private static function imageCreateFromPng() {
        return function($fileName) { return \imagecreatefrompng($fileName); };
    }
    
    /**
     * Return the function that will render an image as a jpeg
     * @return function
     */
    private static function imageRenderAsJpeg() {
        return function($image) { \imagejpeg($image, null, 80); };
    }
    
    /**
     * Return the function that will render an image as a gif
     * @return function
     */
    private static function imageRenderAsGif() {
        return function($image) { \imagegif($image); };
    }
    
    /**
     * Return the function that will render an image as a png
     * @return function
     */
    private static function imageRenderAsPng() {
        return function($image) { \imagepng($image); };
    }
}